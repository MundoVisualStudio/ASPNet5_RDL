# _ASP Net 5 Reporte (RDL)_

## Contenido
- Solución ASP NET 5 MVC
- Reporte RDL de Ejemplo
- Script (.sql)


## Paquetes Nuget

```sh
- AspNetCore.Reporting [2.1.0]
```
## Videos Relacionados
| Nombre | Enlace |
| ------ | ------ |
| ASP Net 5 Reporte RDL | ▶️[Enlace](https://www.youtube.com/watch?v=CoMeJiJcVfs) |
| ASP Net 5 Image en Reporte RDL | ▶️[Enlace](https://www.youtube.com/watch?v=CuBZWOaCDDE)

## Redes Sociales
| Plugin | README |
| ------ | ------ |
| Página de Facebook | 👥 [Enlace](https://www.facebook.com/MundoVisualStudio) |
| Facebook | 👥 [Enlace](https://www.facebook.com/danimedina159) |
| Telegram | 💬 [Enlace](https://t.me/MundoVisualStudio) |



## License
[GoldTech](https://corp-goldtech.com/)
