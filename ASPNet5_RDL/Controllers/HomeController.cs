﻿using ASPNet5_RDL.Models;
using AspNetCore.Reporting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace AC_RDL.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Imprimir()
        {
            //System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            //var dt = new DataTable();
            //dt.Columns.Add("Id");
            //dt.Columns.Add("Ruc");
            //dt.Columns.Add("NombreDeSocio");

            //for (int i = 1; i < 101; i++)
            //{
            //    dt.Rows.Add(i, "RUC", "[NOMBRE DE SOCIO]");
            //}

            //var _params = new Dictionary<string, string> {
            //    { "path_image", @"C:\Archivos Demo\imagen.jpg" },
            //    { "base64_image", Convert.ToBase64String(System.IO.File.ReadAllBytes(@"C:\Archivos Demo\imagen.jpg")) },
            //};

            ////var _source = new Dictionary<string, object> {
            ////	{ "dtSocio", dt },
            ////};

            //var b = AspNetCore.Reporting.RDL.Create(@"C:\Archivos DEMO\rptEjemplo.rdl", _params);
            //return File(b, "application/pdf");

            var rpt = new MVSReport(@"C:\Archivos DEMO\rptEjemplo.rdl");
            rpt.EnableExternalImages = true;
            rpt.AddParam("path_image", @"C:\Archivos Demo\imagen.jpg");
            rpt.AddParam("base64_image", Convert.ToBase64String(System.IO.File.ReadAllBytes(@"C:\Archivos Demo\imagen.jpg")));
            var bb = rpt.Create();
            return File(bb, "application/pdf");


        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

