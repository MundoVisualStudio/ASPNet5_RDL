﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    public class MVSReport
    {
        private Type typerpt = typeof(AspNetCore.Reporting.LocalReport);

        private AspNetCore.Reporting.LocalReport rpt { get; set; } = null;
        private Dictionary<string, string> @params { get; set; } = new Dictionary<string, string>();
        private Dictionary<string, object> @source { get; set; } = new Dictionary<string, object>();
        private Reflection.BindingFlags flags { get; set; } = Reflection.BindingFlags.Instance | Reflection.BindingFlags.Static | Reflection.BindingFlags.Public | Reflection.BindingFlags.NonPublic;
        //private Reflection.BindingFlags flags { get; set; } = (Reflection.BindingFlags)(4 | 8 | 16 | 32);
        private IO.FileInfo fi { get; set; } = null;
        private string filepath { get; set; } = null;

        public string ConnectionString { get; set; }
        public bool EnableExternalImages { get; set; } = false;
        public MVSReport(string filepath)
        {
            this.filepath = filepath;
        }

        public void AddDataSource(string dataSetName, object dataSource)
        {
            this.@source.Add(dataSetName, dataSource);
        }
        public void AddParam(string key, string value)
        {
            this.@params.Add(key, value);
        }

        public byte[] Create()
        {
            byte[] result = null;
            try
            {
                fi = new System.IO.FileInfo(filepath);
                if (fi.Exists)
                {
                    string pathtemp = System.IO.Path.GetTempPath() + @"AspNetCore.Reporting.RDL\";
                    string filepathtemp = pathtemp + fi.Name;
                    //string filepathtemp = System.IO.Path.GetTempFileName();
                    System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                    if (Empty(this.ConnectionString))
                    {
                        filepathtemp = filepath;
                    }
                    else
                    {
                        var b = System.IO.File.ReadAllBytes(filepath);
                        var ms = new System.IO.MemoryStream(b);
                        var xDoc = System.Xml.Linq.XDocument.Load(filepath);
                        var CElement = xDoc.Root.Descendants($"{{http://schemas.microsoft.com/sqlserver/reporting/2016/01/reportdefinition}}ConnectString").SingleOrDefault();
                        if (CElement != null)
                        {
                            CElement.Value = this.ConnectionString;
                            xDoc.Save(filepathtemp);
                        }
                    }
                    this.rpt = new AspNetCore.Reporting.LocalReport(filepathtemp);
                    if (@source != null && @source.Count > 0)
                    {
                        foreach (var item in @source)
                        {
                            this.rpt.AddDataSource(item.Key, item.Value);
                        }
                    }

                    if (this.EnableExternalImages)
                    {
                        var field = typerpt.GetField("localReport", this.flags);
                        var internalLocalReport = field.GetValue(this.rpt);
                        var typeinternal = internalLocalReport.GetType();
                        var propEnableExternalImages = typeinternal.GetProperty("EnableExternalImages", this.flags);
                        propEnableExternalImages.SetValue(internalLocalReport, true);
                    }
                    var rr = this.rpt.Execute(AspNetCore.Reporting.RenderType.Pdf, 1, @params);
                    result = rr.MainStream;
                }
                else
                {
                    throw new Exception($"El archivo: '{filepath}' no es accesible.");
                }                
            }
            catch (Exception ex)
            {
                var _ = ex.Message;
            }
            return result;
        }


        #region Otros Métodos

        private static bool Empty(object Valor)
        {
            string valueSTR;
            var result = true;
            try
            {
                if (Valor != null)
                {
                    Type tp = Valor.GetType();
                    valueSTR = Convert.ToString(Valor);
                    if (Valor == DBNull.Value || string.IsNullOrEmpty(valueSTR) || string.IsNullOrWhiteSpace(valueSTR))
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception)
            {
                result = true;
            }
            return result;
        }
        private string Value(object Valor, string DefaultValue = "")
        {
            string NuevoValor;
            try
            {
                NuevoValor = Convert.ToString(Valor);
                if (NuevoValor == "" || Valor == null || Valor == DBNull.Value)
                {
                    NuevoValor = DefaultValue;
                }
            }
            catch (Exception)
            {
                NuevoValor = DefaultValue;
            }
            return NuevoValor;
        }
        private string ValueTrim(object Valor, string DefaultValue = "")
        {
            string NuevoValor = Value(Valor, DefaultValue);
            NuevoValor = NuevoValor.Trim();
            return NuevoValor;
        }

        #endregion

    }
}
