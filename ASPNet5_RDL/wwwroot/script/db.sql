USE master 
GO
IF DB_ID('dbtiendaseym')IS NOT NULL
BEGIN
	ALTER DATABASE dbtiendaseym SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE dbtiendaseym
END
GO
CREATE DATABASE dbtiendaseym
GO
USE dbtiendaseym
GO
CREATE TABLE dbo.glob_socio(
Id BIGINT IDENTITY PRIMARY KEY,
Ruc VARCHAR(11),
NombreDeSocio VARCHAR(300),
ApellidoPaterno VARCHAR(300),
ApellidoMaterno VARCHAR(300),
RazonSocial VARCHAR(300),
NombreComercial VARCHAR(300),
Telefono1 VARCHAR(100),
Telefono2 VARCHAR(100),
Correo VARCHAR(300),
Direccion VARCHAR(300),
Estado BIT,
Usuario NVARCHAR(300) UNIQUE,
[Password] NVARCHAR(300),
Observacion VARCHAR(300),
Foto IMAGE,
FechaDeRegistro DATETIME DEFAULT GETDATE(),
FechaDeModificacion DATETIME,
Image VARBINARY(MAX))
GO
INSERT dbo.glob_socio ( Ruc, NombreDeSocio, ApellidoPaterno, ApellidoMaterno, Estado, Usuario, Password )
VALUES ( '00000000000', 'Invitado', 'AP', 'AM', 1, '00000000000', '123' )
GO
USE master;
GO
IF EXISTS (SELECT 'A' FROM sys.databases WHERE name = 'dbtiendaseym2')
BEGIN
	ALTER DATABASE dbtiendaseym2 SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
	DROP DATABASE dbtiendaseym2;
END;
GO
CREATE DATABASE dbtiendaseym2;
GO
USE dbtiendaseym2;
GO
CREATE TABLE dbo.glob_socio(
Id BIGINT IDENTITY PRIMARY KEY,
Ruc VARCHAR(11),
NombreDeSocio VARCHAR(300),
ApellidoPaterno VARCHAR(300),
ApellidoMaterno VARCHAR(300),
RazonSocial VARCHAR(300),
NombreComercial VARCHAR(300),
Telefono1 VARCHAR(100),
Telefono2 VARCHAR(100),
Correo VARCHAR(300),
Direccion VARCHAR(300),
Estado BIT,
Usuario NVARCHAR(300) UNIQUE,
[Password] NVARCHAR(300),
Observacion VARCHAR(300),
Foto IMAGE,
FechaDeRegistro DATETIME DEFAULT GETDATE(),
FechaDeModificacion DATETIME,
Image VARBINARY(MAX))
GO
INSERT dbo.glob_socio
(Ruc, NombreDeSocio, ApellidoPaterno, ApellidoMaterno, RazonSocial, NombreComercial, Telefono1, Telefono2, Correo, Direccion, Estado, Usuario, Password, Observacion, FechaDeRegistro, FechaDeModificacion, Image)
SELECT
Ruc,
NombreDeSocio,
ApellidoPaterno,
ApellidoMaterno,
RazonSocial,
NombreComercial,
Telefono1,
Telefono2,
Correo,
Direccion,
Estado,
Usuario,
Password,
Observacion,
FechaDeRegistro,
FechaDeModificacion,
Image
FROM dbtiendaseym.dbo.glob_socio;

UPDATE dbo.glob_socio SET NombreDeSocio = 'FFF' WHERE Id = 1;

/*IMPORTANTE CAMBIAR LA RUTA DE IMAGEN*/
--UPDATE dbo.glob_socio SET
--Foto = (SELECT TOP (1) * FROM OPENROWSET(BULK 'D:\MVS\Videos\imagen\logo.png', SINGLE_BLOB) AS Foto)
--WHERE Id = 1

SELECT Foto FROM dbo.glob_socio WHERE Id = 1

